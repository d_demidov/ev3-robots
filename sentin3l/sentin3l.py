#!/usr/bin/python -u
import os, sys, time
from random import randint
from ev3dev import *
from ev3dev_utils.motors import *

class sentin3l:
    def __init__(self):
        self.dir = os.path.dirname(sys.argv[0])

        self.lm = large_motor(OUTPUT_B)
        self.rm = large_motor(OUTPUT_C)
        self.mm = medium_motor()

        self.ts = touch_sensor()
        self.cs = color_sensor()
        self.ir = infrared_sensor()

        self.reset_legs()

    def reset_legs(self):
        print("Resetting legs position...")
        for m in [self.lm, self.rm]:
            run_for(ever=True, motor=m, power=10, regulation_mode=motor.mode_off)
            time.sleep(0.6)

            while m.pulses_per_second() > 3:
                time.sleep(0.1)

            m.stop()

        for i in range(3):
            run_for(ever=True, motor=self.lm, power=15, regulation_mode=motor.mode_off)
            run_for(ever=True, motor=self.rm, power=15, regulation_mode=motor.mode_off)

            time.sleep(0.2)

            run_for(ever=True, motor=self.lm, power=-15, regulation_mode=motor.mode_off)
            run_for(ever=True, motor=self.rm, power=-15, regulation_mode=motor.mode_off)

            time.sleep(0.2)

        self.lm.stop()
        self.rm.stop()
        print("Done.")

    def walk_fwd(self, prox):
        self.reset_legs()
        sound.play(self.dir + '/walking.wav', True)

        self.cs.mode = color_sensor.mode_ambient
        run_for(ever=True, motor=self.mm, power=20, regulation_mode=motor.mode_on)

        self.ir.mode = infrared_sensor.mode_proximity
        while self.ir.value() > prox:
            for m in [self.lm, self.rm]:
                run_for(degrees=180, power=40, motor=m,
                        regulation_mode=motor.mode_on,
                        stop_mode=motor.stop_mode_brake,
                        wait=False
                        )

            while self.lm.running() and self.rm.running():
                time.sleep(0.1)

        self.lm.stop()
        self.rm.stop()
        self.mm.stop()

    def laser(self):
        sound.play(self.dir + '/shooting.wav', True)

        self.cs.mode = color_sensor.mode_ambient

        sound.play(self.dir + '/shoot.wav', False)
        for i in range( randint(3, 6) ):
            self.cs.mode = color_sensor.mode_reflect
            run_for(degrees=180, motor=self.mm, power=100,
                    regulation_mode=motor.mode_on,
                    stop_mode=motor.stop_mode_brake
                    )
            self.cs.mode = color_sensor.mode_reflect

        time.sleep(0.3)

    def turn(self, prox, seconds):
        sound.play(self.dir + '/turning.wav', True)

        d = 180 * (1 -2 * randint(0,1))

        self.ir.mode = infrared_sensor.mode_proximity

        tic = time.time()
        while self.ir.value() < prox and (time.time() - tic) < seconds:
            run_for(degrees=d, power=30, motor=self.lm,
                    regulation_mode=motor.mode_on,
                    stop_mode=motor.stop_mode_brake,
                    wait=False
                    )
            run_for(degrees=-d, power=30, motor=self.rm,
                    regulation_mode=motor.mode_on,
                    stop_mode=motor.stop_mode_brake,
                    wait=False
                    )

            while self.lm.running() and self.rm.running():
                time.sleep(0.1)

        self.lm.stop()
        self.rm.stop()

    def make_program(self):
        sound.play(self.dir + '/awaiting.wav', True)

        led.green_off()
        led.red_on()
        self.cs.mode = color_sensor.mode_color

        def exec_walk():
            self.walk_fwd(30)

        def exec_laser():
            self.laser()

        def exec_turn():
            self.turn(60, 10)

        self.program = []

        blk = 1
        red = 5
        wht = 6

        while not button.enter.pressed():
            while not self.ts.value() and not button.enter.pressed():
                time.sleep(0.1)

            if self.ts.value():
                clr = self.cs.value()

                if clr == blk:
                    sound.play(self.dir + '/black.wav', True)
                    self.program.append(exec_turn)
                elif clr == red:
                    sound.play(self.dir + '/red.wav', True)
                    self.program.append(exec_laser)
                elif clr == wht:
                    sound.play(self.dir + '/white.wav', True)
                    self.program.append(exec_walk)
                else:
                    sound.speak('unknown command')

                while self.ts.value():
                    time.sleep(0.1)

        led.green_on()
        led.red_off()

    def run_program(self):

        for f in self.program:
            f()

s = sentin3l()

while True:
    s.make_program()
    s.run_program()
