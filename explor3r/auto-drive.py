#!/usr/bin/python
#
# Controls a robot with attached infrared and touch sensors and
# driven by two large motors.
#
# Moves forward until an object is met, then turns and
# continues to run in the other direction.
# Halts when touch sensor is activated.

import time
from ev3dev import *
from ev3dev_utils.motors import *

lmotor = large_motor(OUTPUT_C)
rmotor = large_motor(OUTPUT_B)
irsens = infrared_sensor()
ts     = touch_sensor()

irsens.mode = infrared_sensor.mode_proximity

while True:
    distance = irsens.value()
    if distance < 0: break

    if ts.value():
        sound.tone(100, 100)
        drive_for(seconds=1, left=lmotor, right=rmotor, power=-90,
                regulation_mode='on', dir=50)
        time.sleep(0.01)
    elif distance > 40:
        drive_for(ever=True, left=lmotor, right=rmotor)
        time.sleep(0.01)
    else:
        sound.tone(500, 100)
        while irsens.value() < 70:
            drive_for(ever=True, left=lmotor, right=rmotor, dir=-100)
            time.sleep(0.01)
