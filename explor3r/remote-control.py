#!/usr/bin/python
#
# Remotely controls a robot with infrared and touch sensor attached
# and driven by two large motors.
#
# Halts when touch sensor is activated.

import time
from ev3dev import *
from ev3dev_utils.motors import *
from ev3dev_utils.lcd import LCD

lcd = LCD()

# lcd.draw returns a PIL.ImageDraw handle
lcd.draw.ellipse(( 20, 20,  60, 60))
lcd.draw.ellipse((118, 20, 158, 60))
lcd.draw.arc((20, 80, 158, 100), 0, 180)

# Update lcd display
lcd.update()


rmotor = large_motor(OUTPUT_B)
lmotor = large_motor(OUTPUT_C)

ir = infrared_sensor()
rc = remote_control(ir)
ts = touch_sensor()

def make_roll(m, p):
    def roll(state):
        if state:
            if ts.value() and p > 0:
                m.stop()
                sound.tone(100, 100)
            else:
                run_for(ever=True, motor=m, power=p*90, stop_mode='brake',
                    regulation_mode='on')
        else:
            m.stop()
    return roll

rc.on_red_up   (make_roll(lmotor,  1))
rc.on_red_down (make_roll(lmotor, -1))
rc.on_blue_up  (make_roll(rmotor,  1))
rc.on_blue_down(make_roll(rmotor, -1))

sound.tone(500, 100)

while True:
    if ts.value():
        sound.tone(100, 100)
        drive_for(seconds=0.5, left=lmotor, right=rmotor, power=-90, wait=True)

    if not rc.process(): time.sleep(0.01)

