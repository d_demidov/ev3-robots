from ev3dev import *
from time import sleep
from random import randint
import os, sys

def choose(a, b):
    if randint(0,1):
        return a
    else:
        return b

class ev3storm:
    color = [
            'none',
            'black',
            'blue',
            'green',
            'yellow',
            'red',
            'white',
            'brown'
            ]

    def __init__(self):
        self.dir = os.path.dirname(sys.argv[0])

        self.lm = large_motor('outB')
        self.rm = large_motor('outC')
        self.mm = medium_motor()

        self.ir = infrared_sensor()
        self.ts = touch_sensor()
        self.cs = color_sensor()

        assert self.lm.connected, 'Left large motor (port C) not connected'
        assert self.lm.connected, 'Right large motor (port B) not connected'
        assert self.lm.connected, 'Medium motor not connected'
        assert self.ir.connected, 'Infrared sensor not connected'
        assert self.ts.connected, 'Touch sensor not connected'
        assert self.cs.connected, 'Color sensor not connected'

        self.cs.mode = 'COL-COLOR'

        self.lm.speed_regulation_enabled = 'on'
        self.lm.stop_command = 'brake'
        self.lm.position = 0

        self.rm.speed_regulation_enabled = 'on'
        self.rm.stop_command = 'brake'
        self.rm.position = 0

        self.mm.speed_regulation_enabled = 'on'
        self.mm.position = 0
        self.mm.stop_command = 'brake'
        self.mm.speed_sp = 1200

    def shoot(self, dir = 1):
        sound.play( self.dir + '/explosion.wav')
        self.mm.position_sp = -1080 * dir
        self.mm.command = 'run-to-rel-pos'
        while 'running' in self.mm.state: sleep(0.1)

    def circle(self):
        sound.play( self.dir + '/laugh.wav')

        tm = 3000

        lp,rp = steering(choose(60,-60),900)
        self.lm.speed_sp = lp
        self.rm.speed_sp = rp
        self.lm.time_sp = tm
        self.rm.time_sp = tm
        self.lm.command = 'run-timed'
        self.rm.command = 'run-timed'

        sleep(tm/1000)
    def crazy_dance(self):
        sound.play( self.dir + '/laugh.wav')
        p = choose(180,-180)
        for i in [1, -1, 1, -1]:
            self.lm.position_sp =  i * p
            self.rm.position_sp = -i * p
            self.lm.speed_sp = 900
            self.rm.speed_sp = 900

            self.lm.command = 'run-to-rel-pos'
            self.rm.command = 'run-to-rel-pos'

            while ('running' in self.lm.state) and ('running' in self.rm.state):
                sleep(0.1)

        tm = 2500
        self.lm.speed_sp = -900
        self.rm.speed_sp = 900
        self.lm.time_sp = tm
        self.rm.time_sp = tm
        self.lm.command = 'run-timed'
        self.rm.command = 'run-timed'

        sleep(tm/1000)

    def remote(self):
        rc = remote_control(self.ir)

        def process(state):
            if state & rc.buttons.red_up and state & rc.buttons.red_down:
                self.lm.command = 'stop'
                self.shoot(1)
                return

            if state & rc.buttons.blue_up and state & rc.buttons.blue_down:
                self.rm.command = 'stop'
                self.shoot(-1)
                return

            def roll(m, p):
                m.speed_sp = p * 900
                m.command = 'run-forever'

            if state & rc.buttons.red_up:
                roll(self.lm, 1)
            elif state & rc.buttons.red_down:
                roll(self.lm, -1)
            else:
                self.lm.command = 'stop'

            if state & rc.buttons.blue_up:
                roll(self.rm, 1)
            elif state & rc.buttons.blue_down:
                roll(self.rm, -1)
            else:
                self.rm.command = 'stop'

        rc.on_state_change(process)

        while True:
            if self.ts.value():
                self.shoot(-1)
                sleep(1)

            clr = ev3storm.color[self.cs.value()]

            if clr == 'red':
                self.crazy_dance()
            elif clr == 'blue':
                self.circle()
            elif clr == 'green':
                sound.play( self.dir + '/r2d2.wav')
                sleep(1)
            elif clr == 'white':
                sound.play( self.dir + '/rooster.wav')
                sleep(1)

            rc.process()
            sleep(0.1)

