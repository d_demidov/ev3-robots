#!/usr/bin/python

from ev3dev import *
from time import sleep

m = large_motor()
m.speed_regulation_enabled = 'on'
m.speed_sp = 600
m.set_command('run-forever')

b = button.back

while not b.pressed: sleep(0.1)
