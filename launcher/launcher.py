#!/usr/bin/python

from time import sleep
from math import exp
from ev3dev import *

class launcher:
    def __init__(self):
        self.mm = medium_motor()
        self.lm = large_motor()
        self.ir = infrared_sensor()
        self.ts = touch_sensor()

        self.mm.reset()
        self.lm.reset()

        self.mm.run_timed(
                time_sp=1000,
                speed_regulation_enabled='off',
                duty_cycle_sp=50
                )

        self.lm.run_timed(
                time_sp=1000,
                speed_regulation_enabled='off',
                duty_cycle_sp=-40
                )
        sleep(1)

        self.mm.position=250
        self.lm.position=0

        self.mm.speed_regulation_enabled = 'on'

    def scan_range(self, pos, speed):
        if pos < (pos[0] + pos[1]) / 2:
            start_pos,end_pos = pos
        else:
            start_pos,end_pos = reversed(pos)

        self.mm.run_to_abs_pos(
                position_sp=start_pos,
                speed_sp=300,
                stop_command='brake'
                )
        while 'running' in self.mm.state: sleep(0.1)

        min_val=self.ir.value()
        min_pos=self.mm.position

        self.mm.run_to_abs_pos(
                position_sp=end_pos,
                speed_sp=speed,
                stop_command='brake'
                )

        while 'running' in self.mm.state:
            val = self.ir.value()
            pos = self.mm.position
            if val < min_val:
                min_val = val
                min_pos = pos

            sleep(0.01)

        return min_pos

    def aim(self):
        approx_1 = self.scan_range((50,200), 200)
        approx_2 = self.scan_range((approx_1 - 20, approx_1 + 20), 100)

        self.mm.run_to_abs_pos(
                position_sp=approx_2,
                speed_sp=100,
                stop_command='brake'
                )

        while 'running' in self.mm.state: sleep(0.1)

        print('Target at', approx_2, self.ir.value())

    def shoot(self, power=900):
        self.lm.run_to_abs_pos(
                position_sp=120,
                speed_regulation_enabled='on',
                speed_sp=power,
                stop_command='brake'
                )
        sleep(1)
        self.lm.run_timed(
                time_sp=1000,
                speed_regulation_enabled='off',
                duty_cycle_sp=-40
                )
        sleep(1)

    def cal(self, power):
        print('Target at', self.ir.value())
        self.shoot(power)

if __name__ == "__main__":
    l = launcher()

    while True:
        while not l.ts.value(): sleep(0.1)

        l.aim()
        d = l.ir.value()
        l.shoot(int(250 + 8 * d + 0.07 * exp(0.2 * d)))
