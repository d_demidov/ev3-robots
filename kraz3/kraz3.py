#!/usr/bin/python

from time import sleep
from ev3dev import *
import os, sys

pwd = os.path.dirname(sys.argv[0])

class kraz3:
    def __init__(self):
        self.lm = large_motor(OUTPUT_C)
        self.rm = large_motor(OUTPUT_B)
        self.mm = medium_motor()

        self.ts = touch_sensor()
        self.ir = infrared_sensor()

        self.mm.speed_regulation_enabled = 'on'
        self.lm.speed_regulation_enabled = 'on'
        self.rm.speed_regulation_enabled = 'on'

        self.mm.stop_command = 'brake'
        self.lm.stop_command = 'brake'
        self.rm.stop_command = 'brake'

        self.lm.polarity = 'inverted'
        self.rm.polarity = 'inverted'

    def trick(self):
        self.lm.stop()
        self.rm.stop()

        sound.play(pwd + '/laugh.wav')
        self.mm.run_timed(time_sp=2000, speed_sp=1000)

        for p in [800, -800, 800, -800]:
            self.lm.run_timed(time_sp=250, speed_sp=p)
            self.rm.run_timed(time_sp=250, speed_sp=-p)
            sleep(0.25)

    def search(self):
        self.ir.mode = 'IR-SEEK'
        sleep(0.5)

        while True:
            head,dist = self.ir.value(0), self.ir.value(1)

            if dist < 10:
                sound.play(pwd + '/laugh.wav')
                break

            for m,p in zip((self.lm, self.rm), steering(head * 3, 750)):
                m.run_forever(speed_sp=p)

        self.lm.stop()
        self.rm.stop()
        self.ir.mode = 'IR-REMOTE'

    def remote(self):
        rc = remote_control(self.ir)

        def process(state):
            if state & rc.buttons.red_up and state & rc.buttons.red_down:
                self.trick()
                return

            if state & rc.buttons.blue_up and state & rc.buttons.blue_down:
                self.trick()
                return
        
            if state & rc.buttons.red_up:
                self.lm.run_forever(speed_sp=750)
            elif state & rc.buttons.red_down:
                self.lm.run_forever(speed_sp=-750)
            else:
                self.lm.stop()

            if state & rc.buttons.blue_up:
                self.rm.run_forever(speed_sp=750)
            elif state & rc.buttons.blue_down:
                self.rm.run_forever(speed_sp=-750)
            else:
                self.rm.stop()

            if state & rc.buttons.beacon:
                self.search()

        rc.on_state_change(process)

        while True:
            rc.process()

            if self.ts.value():
                self.trick()

            sleep(0.01)


if __name__ == "__main__":
    k = kraz3()
    k.remote()


