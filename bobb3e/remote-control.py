#!/usr/bin/python
#
# Remotely controls a robot with infrared and touch sensor attached
# and driven by two large motors.
#
# Halts when touch sensor is activated.

import time
from bobb3e import *

b = bobb3e()
rc1 = remote_control(b.ir, 1)
rc2 = remote_control(b.ir, 2)

def make_roll(m, p):
    def roll(state):
        if state:
            run_for(ever=True, motor=m, power=-p)
        else:
            m.stop()
    return roll

rc1.on_red_up   (make_roll(b.lm,  80))
rc1.on_red_down (make_roll(b.lm, -80))
rc1.on_blue_up  (make_roll(b.rm,  80))
rc1.on_blue_down(make_roll(b.rm, -80))

def make_gun_control(direction):
    def gun_control(state):
        if (state):
            getattr(b, 'shoot_' + direction)()

    return gun_control

rc2.on_red_up  (make_gun_control('up'))
rc2.on_red_down(make_gun_control('down'))

while not b.ts.value():
    rc1.process()
    rc2.process()
    time.sleep(0.01)

