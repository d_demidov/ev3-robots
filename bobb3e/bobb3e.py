import time
from random import randint
from ev3dev import *
from ev3dev_utils.motors import *

class bobb3e:
    def __init__(self):
        self.lm = large_motor(OUTPUT_B)
        self.rm = large_motor(OUTPUT_C)
        self.mm = medium_motor()

        self.ir = infrared_sensor()
        self.cs = color_sensor()
        self.ts = touch_sensor()

        self.program = []

    def forward(self, prox=30):
        self.ir.mode = infrared_sensor.mode_proximity
        time.sleep(0.5)
        print('forward')

        while self.ir.value() > prox:
            for m in [self.lm, self.rm]:
                run_for(ever=True, motor=m, power=-80,
                        regulation_mode=motor.mode_on,
                        stop_mode=motor.stop_mode_brake)
            time.sleep(0.1)

        self.lm.stop()
        self.rm.stop()

    def turn(self, degrees):
        self.lm.position = 0
        self.rm.position = 0

        run_for(degrees=degrees, motor=self.lm, power=100,
                regulation_mode=motor.mode_on,
                stop_mode=motor.stop_mode_brake, wait=False
                )
        run_for(degrees=-degrees, motor=self.rm, power=100,
                regulation_mode=motor.mode_on,
                stop_mode=motor.stop_mode_brake, wait=True
                )

    def turn_left(self, degrees=640):
        print('left')
        self.turn(degrees)

    def turn_right(self, degrees=640):
        print('right')
        self.turn(-degrees)

    def backup(self, seconds=0.8):
        print('back')
        drive_for(seconds=seconds, left=self.lm, right=self.rm, power=100,
                regulation_mode=motor.mode_on,
                stop_mode=motor.stop_mode_brake
                )

    def shoot_down(self, n=1):
        print('shoot down')
        self.mm.position = 0
        for i in range(n):
            run_for(degrees=1080, motor=self.mm, power=100,
                    regulation_mode=motor.mode_on,
                    stop_mode=motor.stop_mode_brake)


    def shoot_up(self, n=1):
        print('shoot up')
        self.mm.position = 0
        for i in range(n):
            run_for(degrees=-1080, motor=self.mm, power=100,
                    regulation_mode=motor.mode_on,
                    stop_mode=motor.stop_mode_brake)

    def hunt(self, kp=3, ki=0.5, kd=2):
        print('hunt')
        self.ir.mode = infrared_sensor.mode_ir_seeker
        time.sleep(0.1)

        tic = time.time()
        bored = randint(10,30)

        while self.ir.value(1) == -128:
            time.sleep(0.1)

            if time.time() - tic >= bored:
                sound.speak('Where are you?')
                tic = time.time()
                bored = randint(10,30)

        last_error = 0
        integral   = 0

        while self.ir.value(1) > 20:
            error = self.ir.value(0)
            dist  = self.ir.value(1)

            integral   = 0.5 * integral + error
            derivative = error - last_error
            last_error = error

            correction = kp * error + ki * integral + kd * derivative

            drive_for(ever=True, left=self.lm, right=self.rm,
                    power=-80, dir=correction,
                    regulation_mode=motor.mode_on,
                    stop_mode=motor.stop_mode_brake
                    )

            time.sleep(0.1)

        while abs(self.ir.value(0)) > 0:
            error = self.ir.value(0)

            integral   = 0.5 * integral + error
            derivative = error - last_error
            last_error = error

            correction = 3 * (kp * error + ki * integral + kd * derivative)

            if correction == 0:
                break
            elif correction > 0:
                direction=100
            else:
                direction=-100

            drive_for(seconds=0.2, left=self.lm, right=self.rm,
                    power=-abs(correction), dir=direction,
                    regulation_mode=motor.mode_on,
                    stop_mode=motor.stop_mode_brake
                    )

        self.lm.stop()
        self.rm.stop()

    def make_program(self):
        sound.speak('At your command', True)

        led.green_off()
        led.red_on()

        def exec_up():
            self.forward()

        def exec_down():
            self.backup()

        def exec_left():
            self.turn_left()

        def exec_right():
            self.turn_right()

        def exec_enter():
            self.hunt()
            self.shoot_down(3)

        self.program = []

        while not self.ts.value():
            while not self.ts.value():
                if button.up.pressed():
                    print('up')
                    self.program.append(exec_up)
                    break
                elif button.down.pressed():
                    print('down')
                    self.program.append(exec_down)
                    break
                elif button.left.pressed():
                    print('left')
                    self.program.append(exec_left)
                    break
                elif button.right.pressed():
                    print('right')
                    self.program.append(exec_right)
                    break
                elif button.enter.pressed():
                    print('enter')
                    self.program.append(exec_enter)
                    break

                time.sleep(0.1)

            time.sleep(0.2)

        led.green_on()
        led.red_off()

    def exec_program(self):
        for action in self.program:
            action()

