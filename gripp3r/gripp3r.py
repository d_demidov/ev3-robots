#!/usr/bin/python

from ev3dev import *
from time import sleep

class gripp3r:
    def __init__(self):
        self.lm = large_motor(OUTPUT_B)
        self.rm = large_motor(OUTPUT_C)
        self.mm = medium_motor()
        self.ir = infrared_sensor()

        self.ir.mode = 'IR-PROX'

        self.lm.speed_regulation_enabled = 'on'
        self.rm.speed_regulation_enabled = 'on'
        self.mm.speed_regulation_enabled = 'off'

        self.lm.stop_command = 'brake'
        self.rm.stop_command = 'brake'
        self.mm.stop_command = 'brake'

    def remote(self):
        rc = remote_control(self.ir)

        def process(state):
            if state & rc.buttons.red_up and state & rc.buttons.red_down:
                self.lm.stop()
                self.rm.stop()
                self.mm.run_timed(time_sp=750, duty_cycle_sp=75)
                return

            if state & rc.buttons.blue_up and state & rc.buttons.blue_down:
                self.lm.stop()
                self.rm.stop()
                self.mm.run_timed(time_sp=750, duty_cycle_sp=-75)
                return
        
            if state & rc.buttons.red_up:
                self.lm.run_forever(speed_sp=750)
            elif state & rc.buttons.red_down:
                self.lm.run_forever(speed_sp=-750)
            else:
                self.lm.stop()

            if state & rc.buttons.blue_up:
                self.rm.run_forever(speed_sp=750)
            elif state & rc.buttons.blue_down:
                self.rm.run_forever(speed_sp=-750)
            else:
                self.rm.stop()

        rc.on_state_change(process)


        while True:
            rc.process()
            sleep(0.01)


if __name__ == "__main__":
    g = gripp3r()
    g.remote()

