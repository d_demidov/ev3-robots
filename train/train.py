#!/usr/bin/python

from time import sleep
from ev3dev import *

fm = large_motor(OUTPUT_C)
bm = large_motor(OUTPUT_B)
ir = infrared_sensor()

fm.reset()
bm.reset()

fm.speed_regulation_enabled = 'on'
bm.speed_regulation_enabled = 'on'

fm.stop_command = 'brake'
bm.stop_command = 'brake'

bm.polarity = 'inverted'

def make_roll(p):
    def roll(state):
        if state:
            fm.run_forever(speed_sp=p)
            bm.run_forever(speed_sp=p)
        else:
            fm.stop()
            bm.stop()
    return roll

rc = remote_control(ir)
rc.on_red_up  (make_roll(750))
rc.on_red_down(make_roll(-750))

while True:
    rc.process()
    sleep(0.01)


