#!/usr/bin/python

import os, sys
from time import sleep
from ev3dev import *

pwd = os.path.dirname(sys.argv[0])

fm = large_motor(OUTPUT_C)
bm = large_motor(OUTPUT_B)
ir = infrared_sensor()
ts = touch_sensor()

fm.reset()
bm.reset()

fm.speed_regulation_enabled = 'on'
bm.speed_regulation_enabled = 'on'
fm.ramp_up_sp = 3000
bm.ramp_up_sp = 3000

fm.stop_command = 'brake'
bm.stop_command = 'brake'

bm.polarity = 'inverted'

while True:
    if ts.value():
        sound.play(pwd + '/train.wav')

    if ir.value() < 15:
        sound.tone(100, 1000)
        fm.stop()
        bm.stop()
    else:
        fm.run_forever(speed_sp=750)
        bm.run_forever(speed_sp=750)


