#!/usr/bin/python -u

import os, sys, time
from ev3dev import *
from ev3dev_utils.motors import *

class T_R3X:
    def __init__(self):
        self.dir = os.path.dirname(sys.argv[0])
        sound.play(self.dir + '/roar.wav')

        self.mlegs = large_motor(OUTPUT_A)
        self.mbody = large_motor(OUTPUT_D)
        self.mjaws = medium_motor()

        self.mlegs.regulation_mode = motor.mode_on
        self.mbody.regulation_mode = motor.mode_on
        self.mjaws.regulation_mode = motor.mode_on

        self.mlegs.stop_mode = motor.stop_mode_brake
        self.mjaws.stop_mode = motor.stop_mode_brake
        self.mbody.stop_mode = motor.stop_mode_brake
        self.mbody.ramp_up   = 500
        self.mbody.ramp_down = 500

        self.irsens = infrared_sensor()
        self.irsens.mode = 'IR-PROX'

        self.reset()

    def reset(self):
        run_until(stalled=True, motor=self.mbody, power=20)
        run_until(stalled=True, motor=self.mlegs, power=25)

        self.mlegs.position = 0
        self.mbody.position = 0

        time.sleep(0.2)

        run_until(degrees=-36,  motor=self.mbody, power=20)
        self.mbody.position = 0

        time.sleep(0.2)

        run_until(degrees=-120, motor=self.mlegs, power=25,
                stop_mode=motor.stop_mode_hold)
        self.mlegs.position = 0

        time.sleep(0.2)


        run_until(stalled=True, motor=self.mjaws, power=-30)

        self.mjaws.position = 0

        run_until(degrees=15, motor=self.mjaws, power=30)
        time.sleep(0.5)
        run_until(degrees=5, motor=self.mjaws, power=30)

    def step(self):
        run_for(ever=True, motor=self.mlegs, power=-30)
        time.sleep(0.1)
        self.shift_right()
        self.mlegs.stop()

        time.sleep(0.2)

        run_for(ever=True, motor=self.mlegs, power=30)
        time.sleep(0.1)
        self.shift_left()
        self.mlegs.stop()

        time.sleep(0.2)

    def roar(self):
        sound.play(self.dir + '/roar.wav')
        time.sleep(0.2)
        for i in range(2):
            run_for(seconds=0.1, motor=self.mjaws, power=60)
            time.sleep(0.5)
            run_for(seconds=0.1, motor=self.mjaws, power=-60)

    def right(self):
        run_until(degrees=-50, motor=self.mlegs, power=30)
        run_until(degrees=-15, motor=self.mbody, power=15)

        run_for(degrees=50, motor=self.mlegs, power=50, wait=False)
        run_for(degrees=35, motor=self.mbody, power=50, wait=False)
        time.sleep(0.2)

    def left(self):
        run_until(degrees=50, motor=self.mlegs, power=30)
        run_until(degrees=15, motor=self.mbody, power=15)

        run_for(degrees=-50, motor=self.mlegs, power=50, wait=False)
        run_for(degrees=-40, motor=self.mbody, power=50, wait=False)
        time.sleep(0.2)

    def shift_left(self):
        run_until(degrees=36, motor=self.mbody, power=10)

    def shift_right(self):
        run_until(degrees=-50, motor=self.mbody, power=10)
