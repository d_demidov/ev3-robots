#!/usr/bin/python -u

import time

from ev3dev import *
from ev3dev_utils.motors import *
from t_r3x import *

r = T_R3X()

def dist_to_object():
    r.irsens.mode = 'IR-PROX'
    time.sleep(0.2)
    return r.irsens.value()

def beacon():
    r.irsens.mode = 'IR-SEEK'
    time.sleep(0.2)
    return (r.irsens.value(0), r.irsens.value(1))

class state_machine:
    def idle(self):
        while True:
            if dist_to_object() < 20:
                return 'angry'

            _,dist = beacon()

            if dist >= 0:
                return 'seek'

            time.sleep(0.1)

    def angry(self):
        r.roar()
        return 'idle'

    def seek(self):
        r.roar()

        while True:
            heading,dist = beacon()

            if dist == -128:
                return 'angry'

            if dist < 10:
                return 'eat'

            if abs(heading) < 10:
                r.step()
            elif heading < 0:
                r.left()
            else:
                r.right()

            time.sleep(0.1)

    def eat(self):
        r.roar()
        return 'idle'

    def run(self):
        state = 'idle'
        while True:
            print('State: %s' % state)
            state = getattr(self, state)()
            time.sleep(0.1)

#----------------------------------------------------------------------------
# Its alive!
#----------------------------------------------------------------------------
sm = state_machine()
sm.run()
