#!/usr/bin/python -u
import time
from ev3dev import *
from ev3dev_utils.motors import *

class gooz3:
    def __init__(self):
        self.mlegs = large_motor()
        self.mbody = medium_motor()

        self.tsensor = touch_sensor()
        self.irsensor = infrared_sensor()

    def shift_weight(self, side):
        run_until(motor=self.mbody, power = side * 40,
                regulation_mode=motor.mode_on,
                check=lambda: not self.tsensor.value()
                )

        run_until(motor=self.mbody, power = side * 40,
                regulation_mode=motor.mode_on,
                check=lambda: self.tsensor.value()
                )

    def step(self, side, dir):
        self.shift_weight(side)
        run_for(seconds=1.5, motor=self.mlegs, power=-100*dir,
                regulation_mode=motor.mode_on
                )

    def step_forward(self):
        self.step( 1,  1)
        self.step(-1, -1)

    def step_backward(self):
        self.step( 1, -1)
        self.step(-1,  1)

if __name__ == "__main__":
    g = gooz3()

    g.irsensor.mode = 'IR-PROX'
    while g.irsensor.value() > 30:
        g.step_forward()

