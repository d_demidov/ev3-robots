import sys, time, argparse
from ev3dev import *
from ev3dev_utils.motors import *
from snatch3r import *

#----------------------------------------------------------------------------
# Parse command line
#----------------------------------------------------------------------------
parser = argparse.ArgumentParser(sys.argv[0])
parser.add_argument(
    '--Kp', dest='Kp', type=float, default=3
    )
parser.add_argument(
    '--Ki', dest='Ki', type=float, default=0.1
    )
parser.add_argument(
    '--Kd', dest='Kd', type=float, default=2
    )
args = parser.parse_args(sys.argv[1:])

#----------------------------------------------------------------------------
s = snatch3r()

mid = s.calibrate()

def follow_the_line(stop):
    last_error = 0
    integral   = 0

    while not stop():
        error = mid - s.color_sensor.value()
        integral = integral + error
        derivative = error - last_error
        last_error = error

        correction = args.Kp * error + args.Ki * integral + args.Kd * derivative

        drive_for(s.lmotor, s.rmotor, dir=correction, power=60, ever=True)

        time.sleep(0.01)

    s.stop()

s.ir_sensor.mode = infrared_sensor.mode_proximity
tic = time.time()

follow_the_line(stop=lambda: s.ir_sensor.value() < 5)

path_length = time.time() - tic

sound.speak("Mine! Mine! Mine!")

s.grab()
s.turn_around()

tic = time.time()
follow_the_line(stop=lambda: time.time() - tic > path_length)

s.release()
