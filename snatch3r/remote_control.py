import sys, time
from ev3dev import *
from ev3dev_utils import *
from snatch3r import *

s = snatch3r()

def make_roll(m, p):
    def roll(state):
        if state:
            run_for(m, power=p, ever=True)
        else:
            m.reset()

    return roll

def grab(state):
    if not s.grabbing and state:
        s.grab()

def release(state):
    if not s.grabbing and state:
        s.release()

rc1 = remote_control(s.ir_sensor, 1)
rc1.on_red_up   (make_roll(s.lmotor,  90))
rc1.on_red_down (make_roll(s.lmotor, -90))
rc1.on_blue_up  (make_roll(s.rmotor,  90))
rc1.on_blue_down(make_roll(s.rmotor, -90))

rc2 = remote_control(s.ir_sensor, 2)
rc2.on_red_up  (grab)
rc2.on_red_down(release)

s.ir_sensor.mode = infrared_sensor.mode_ir_remote
while True:
    rc1.process()
    rc2.process()

    time.sleep(0.01)

