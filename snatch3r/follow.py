import sys, time, argparse
from ev3dev import *
from ev3dev_utils.motors import *
from snatch3r import *

#----------------------------------------------------------------------------
# Parse command line
#----------------------------------------------------------------------------
parser = argparse.ArgumentParser(sys.argv[0])
parser.add_argument(
    '--Kp', dest='Kp', type=float, default=3
    )
parser.add_argument(
    '--Ki', dest='Ki', type=float, default=0.5
    )
parser.add_argument(
    '--Kd', dest='Kd', type=float, default=2
    )
args = parser.parse_args(sys.argv[1:])

#----------------------------------------------------------------------------
s = snatch3r()

s.ir_sensor.mode = infrared_sensor.mode_ir_seeker

last_error = 0
integral   = 0

while s.ir_sensor.value(1) > 0:
    error = s.ir_sensor.value(0)
    dist  = s.ir_sensor.value(1)

    integral = 0.5 * integral + error
    derivative = error - last_error
    last_error = error

    correction = args.Kp * error + args.Ki * integral + args.Kd * derivative

    drive_for(ever=True, left=s.lmotor, right=s.rmotor, power=dist/2+50, dir=correction)

time.sleep(0.2)
s.reset_motors()

sound.speak("Got you!")

s.grab()
s.turn_around()
s.release()
