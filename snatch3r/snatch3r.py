import sys, time
from ev3dev import *
from ev3dev_utils.motors import *

class snatch3r:
    def __init__(self):
        def check_device(dev, name):
            if dev.connected():
                self.log("Got %s at %s" % (name, dev.port_name()))
                return dev
            else:
                raise Exception("%s not connected!" % name)

        self.lmotor = check_device(large_motor(OUTPUT_B),  'large motor')
        self.rmotor = check_device(large_motor(OUTPUT_C),  'large motor')
        self.mmotor = check_device(medium_motor(OUTPUT_A), 'medium motor')

        self.touch_sensor = check_device(touch_sensor(),    'touch sensor')
        self.ir_sensor    = check_device(infrared_sensor(), 'IR sensor')
        self.color_sensor = check_device(color_sensor(),    'color sensor')

        self.color_sensor.mode = color_sensor.mode_reflect

        self.reset_motors()
        self.grabbing = False

    def __del__(self):
        self.reset_motors()

    def log(self, msg):
        print(msg)
        sys.stdout.flush()

    def reset_motors(self):
        self.lmotor.reset()
        self.mmotor.reset()
        self.rmotor.reset()

        self.lmotor.regulation_mode = motor.mode_on
        self.rmotor.regulation_mode = motor.mode_on
        self.mmotor.regulation_mode = motor.mode_on

        self.lmotor.stop_mode = motor.stop_mode_brake
        self.rmotor.stop_mode = motor.stop_mode_brake
        self.mmotor.stop_mode = motor.stop_mode_brake

    def grab(self):
        self.log("grabbing")

        self.grabbing = True
        run_until(self.mmotor, power=80, check=lambda: self.touched())
        self.grabbing = False

    def release(self):
        self.log("releasing")

        self.grabbing = True
        run_for(self.mmotor, power=80, degrees=-5180)
        while self.mmotor.running(): time.sleep(0.01)
        self.grabbing = False

    def turn_around(self):
        self.log("turning")
        run_for(self.rmotor, power=100, degrees= 900)
        run_for(self.lmotor, power=100, degrees=-900)
        while self.lmotor.running(): time.sleep(0.01)

    def stop(self):
        self.lmotor.stop()
        self.rmotor.stop()

    def touched(self):
        return self.touch_sensor.value()

    def calibrate(self):
        self.log("calibrating")

        def get_reading(color):
            sound.speak("Show me %s!" % color, True)
            while not self.touched(): time.sleep(0.1)

            v = self.color_sensor.value()

            print("%s: %s" %(color, v))
            sound.speak("OK", True)
            return v

        return 0.5 * (get_reading('white') + get_reading('black'))
