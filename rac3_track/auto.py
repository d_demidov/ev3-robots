#!/usr/bin/python

from time import sleep
from ev3dev import *
from random import randint
from ev3dev_utils.motors import *

lm = large_motor(OUTPUT_B)
rm = large_motor(OUTPUT_C)
mm = medium_motor()
ir = infrared_sensor()
ir.mode = 'IR-PROX'

def drive(p):
    drive_for(ever=True, left=lm, right=rm, power=-p*90, regulation_mode='on',
            stop_mode='brake')

def stop():
    lm.stop()
    rm.stop()

def reset():
    run_until(stalled=True, motor=mm)
    mm.position=0
    run_for(degrees=-120, motor=mm, stop_mode='brake', wait=True)
    mm.position=0

def turn(p):
    run_until(stalled=True, motor=mm, power=p*90, stop_mode='brake')
    drive(-1)
    sleep(0.7)
    stop()
    run_until(stalled=True, motor=mm, power=-p*90, stop_mode='brake')
    drive(1)
    sleep(1)
    stop()
    run_until(degrees=0, motor=mm, stop_mode='brake')

def beep():
    for i in range(2):
        sound.tone(100,200)
        sleep(0.2)
        sound.tone(200,100)
        sleep(0.2)

reset()

while True:
    dist = ir.value()
    
    if dist > 40:
        drive(1)
        sleep(0.01)
    else:
        stop()
        beep()
        turn(randint(0,1)*2-1)
