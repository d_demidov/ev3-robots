from ev3dev import *
from ev3dev_utils.motors import *

class track:
    def __init__(self):
        self.lm = large_motor(OUTPUT_B)
        self.rm = large_motor(OUTPUT_C)
        self.mm = medium_motor()
        self.ts = touch_sensor()
        self.ir = infrared_sensor()

    def reset(self):
        run_for(seconds=1.5, motor=self.mm, power=30, regulation_mode='on')
        run_for(degrees=-120, motor=self.mm, power=50, regulation_mode='on',
                stop_mode='brake', wait=True)
        self.mm.position = 0

    def forward(self):
        drive_for(ever=True, left=self.lm, right=self.rm, power=-80,
                regulation_mode='on')

    def stop(self):
        self.lm.stop()
        self.rm.stop()

