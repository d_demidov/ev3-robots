#!/usr/bin/python

from time import sleep
from ev3dev import *
from ev3dev_utils.motors import *

sound.tone(500, 100)

lm = large_motor(OUTPUT_B)
rm = large_motor(OUTPUT_C)
mm = medium_motor()

ir = infrared_sensor()
ir.mode = 'IR-PROX'
rc = remote_control(ir)
ts = touch_sensor()

def make_move(p):
    def move(state):
        if state:
            drive_for(ever=True, left=lm, right=rm, power=-p*90,
                    regulation_mode='on', stop_mode='brake')
        else:
            lm.stop()
            rm.stop()

    return move

def make_turn(p):
    def turn(state):
        if state:
            run_for(ever=True, motor=mm, power=p*65,
                    regulation_mode='off', stop_mode='brake')
        else:
            mm.stop()

    return turn

rc.on_red_up   ( make_move( 1) )
rc.on_red_down ( make_move(-1) )
rc.on_blue_up  ( make_turn( 1) )
rc.on_blue_down( make_turn(-1) )

while True:
    if ts.value(): sound.tone(100, 100)
    ir.mode = 'IR-PROX'
    if ir.value() < 20: sound.tone(200, 100)

    ir.mode = 'IR-REMOTE'
    if not rc.process(): sleep(0.01)

